/* jshint esversion: 6 */

const messages = {
  ALREADY_EXISTS_GUEST: `This guestname is been taken. Choose another one`,
  MISSING_GUEST_NAME: `You must pick guest name`,
};

const getMessage = message => messages[message];

export default { messages, getMessage };
