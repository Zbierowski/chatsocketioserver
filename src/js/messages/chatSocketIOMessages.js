/* jshint esversion: 6 */

export const SEND_GUEST_TO_SERVER = `SERVER_SendGuest`;
export const SEND_MESSAGE_TO_SERVER = `SERVER_SendMessage`;
export const SEND_IF_GUEST_EXISTS = `SERVER_CheckIfGuestExists`;
export const SEND_LOGOUT_REQUEST = `SERVER_SendLogout`;
export const SEND_GUEST_EXISTS = `UI_SendGuestExists`;
export const SHOW_GUESTS_TO_ALL_UI = `UI_ShowGuestToAll`;
export const SHOW_GUEST_TO_UI = `UI_showGuesTo`;
export const SEND_MESSAGE_TO_UI = `UI_sendMessage`;
export const SEND_LOGOUT_TO_UI = `UI_SendLogout`;
export const CONNECT = `Connected to Socket.IO`;
export const DISCONNECT = `disconnect`;
