/* jshint esversion: 6 */
/* global document */
import Chat from '../chat';
import DomManipulation from '../helpers/domManipulation';
import { fromEvent } from 'rxjs';

class ChatUI {
  constructor() {
    this.domManipulation = new DomManipulation();
    this.chatContainer = document.querySelector(`[data-chat-container]`);
    this.logoutButton = document.querySelector(`[data-chat-logout]`);

    this.UI_Logout();
  }

  init(socket) {
    this.chat = new Chat(socket);
    this.UI_PrepareUI();
  }

  UI_PrepareUI() {
    this.domManipulation.hideElement(this.logoutButton);
    this.domManipulation.hideElement(this.chatContainer);
  }

  UI_Logout() {
    const logoutAction = fromEvent(this.logoutButton, `click`);

    logoutAction.subscribe(() => {
      this.chat.SERVER_SendLogoutRequest();
      this.domManipulation.hideElement(this.chatContainer);
    });
  }

  UI_LogoutAction(guest) {
    const guestAttribute = `[data-guest-${guest}]`;
    const guestElement = this.guestList.querySelector(guestAttribute);
    this.domManipulation.removeElement(guestElement);
  }
}

export default ChatUI;
