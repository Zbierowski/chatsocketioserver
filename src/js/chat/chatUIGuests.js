/* jshint esversion: 6 */
/* global document */

import Chat from '../chat';
import DomManipulation from '../helpers/domManipulation';
import { fromEvent } from 'rxjs';
import messages from '../messages/messages';
import { SEND_GUEST_EXISTS } from '../messages/chatSocketIOMessages';

class ChatUIGuests {
  constructor() {
    this.domManipulation = new DomManipulation();
    this.joinFormContainer = document.querySelector(`[data-chat-form-container]`);
    this.joinForm = document.querySelector(`[data-chat-join-form]`);
    this.joinInput = document.querySelector(`[data-chat-join-input]`);
    this.joinError = document.querySelector(`[data-chat-join-error]`);
    this.chatContainer = document.querySelector(`[data-chat-container]`);
    this.guestList = document.querySelector(`[data-chat-guests]`);
    this.guestInfo = document.querySelector('[data-chat-guest-info]');
    this.logoutButton = document.querySelector(`[data-chat-logout]`);

    this.UI_SubmitJoinForm();
  }

  init(socket) {
    this.chat = new Chat(socket);
    this.socket = socket;
  }

  UI_SubmitJoinForm() {
    const submitFormAction = fromEvent(this.joinForm, `submit`);

    submitFormAction.subscribe(event => {
      event.preventDefault();
      this.UI_SendGuest();
    });
  }

  UI_ShowGuestToAll(guests) {
    const guestsArray = Object.values(guests);

    this.domManipulation.clearElement(this.guestList);
    guestsArray.forEach(guest => {
      const guestAttribute = `data-guest-${guest}`;

      this.domManipulation.addElement({
        content: guest,
        elementToAdd: `li`,
        whereToAdd: this.guestList,
        attribute: guestAttribute,
        className: `chat__guestListMember`,
      });
    });
  }

  UI_ShowGuest(guest) {
    this.domManipulation.removeElement(this.joinForm);
    this.UI_SetGuestInfo(guest);
  }

  UI_SendGuest() {
    const guestName = this.joinInput.value;

    this.chat.SERVER_SendIfGuestExists(guestName);
    this.socket.on(SEND_GUEST_EXISTS, ifGuestExists => this.UI_ValidateGuestAndSendToServer(ifGuestExists));
  }

  UI_ValidateGuestAndSendToServer(ifGuestExists) {
    const guestName = this.joinInput.value;

    this.socket.removeAllListeners(SEND_GUEST_EXISTS);

    if (!guestName) {
      this.UI_SetErrorMessage(`MISSING_GUEST_NAME`);
      return;
    }

    if (ifGuestExists) {
      this.UI_SetErrorMessage(`ALREADY_EXISTS_GUEST`);
      return;
    }

    this.chat.SERVER_SendGuest(guestName);
    this.domManipulation.showElement(this.logoutButton, `block`);
    this.domManipulation.showElement(this.chatContainer, `flex`);
  }

  UI_SetErrorMessage(messageType) {
    this.domManipulation.setElementsContent(this.joinError, messages.getMessage(messageType));
  }

  UI_SetGuestInfo(guest) {
    this.domManipulation.setElementsContent(this.guestInfo, guest);
  }
}

export default ChatUIGuests;
