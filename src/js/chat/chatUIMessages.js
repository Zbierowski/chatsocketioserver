/* jshint esversion: 6 */
/* global document */

import Chat from '../chat';
import DomManipulation from '../helpers/domManipulation';
import { fromEvent } from 'rxjs';

class ChatUIGuests {
  constructor() {
    this.domManipulation = new DomManipulation();
    this.messagesForm = document.querySelector(`[data-chat-messages-form]`);
    this.messagesInput = document.querySelector(`[data-chat-messages-input]`);
    this.messagesList = document.querySelector(`[data-chat-messages-list]`);

    this.UI_KeyUpMessagesForm();
    this.UI_SubmitMessageForm();
  }

  init(socket) {
    this.chat = new Chat(socket);
  }

  UI_KeyUpMessagesForm() {
    const messageFormAction = fromEvent(this.messagesForm, `keyup`);

    messageFormAction.subscribe(event => {
      const enterPressed = event.keyCode === 13;
      const enterAndShiftPressed = enterPressed && event.shiftKey;

      if (enterPressed && !enterAndShiftPressed) {
        this.UI_SendMessage();
      }
    });
  }

  UI_SubmitMessageForm() {
    const messageFormSubmit = fromEvent(this.messagesForm, 'submit');

    messageFormSubmit.subscribe(event => {
      event.preventDefault();
      this.UI_SendMessage();
    });
  }

  UI_ShowMessage(messageWithGuest) {

    const message = messageWithGuest.message;
    const guest = messageWithGuest.guest;

    this.domManipulation.addElement({
      content: `<b>${guest}</b> : ${message}`,
      elementToAdd: `li`,
      whereToAdd: this.messagesList,
      className: `chat__messagesItem`,
    });
  }

  UI_SendMessage() {
    const message = this.messagesInput.value;

    if (message) {
      this.chat.SERVER_SendMessage(message);
      this.domManipulation.clearInput(this.messagesInput);
    }
  }
}

export default ChatUIGuests;
