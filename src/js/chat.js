/* jshint esversion: 6 */

import {
  SEND_GUEST_TO_SERVER,
  SEND_IF_GUEST_EXISTS,
  SEND_MESSAGE_TO_SERVER,
  SEND_LOGOUT_REQUEST
} from './messages/chatSocketIOMessages';

class Chat {
  constructor(socket) {
    this.socket = socket;
  }

  SERVER_SendGuest(name) {
    this.socket.emit(SEND_GUEST_TO_SERVER, name);
  }

  SERVER_SendMessage(message) {
    this.socket.emit(SEND_MESSAGE_TO_SERVER, message);
  }

  SERVER_SendIfGuestExists(name) {
    this.socket.emit(SEND_IF_GUEST_EXISTS, name);
  }

  SERVER_SendLogoutRequest() {
    this.socket.emit(SEND_LOGOUT_REQUEST);
  }
}

export default Chat;
