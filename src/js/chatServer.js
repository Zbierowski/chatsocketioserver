/* jshint esversion: 6 */
/* global console */
import io from 'socket.io';
import {
  SEND_GUEST_TO_SERVER,
  SEND_IF_GUEST_EXISTS,
  SEND_MESSAGE_TO_SERVER,
  SEND_LOGOUT_REQUEST,
  SEND_GUEST_EXISTS,
  SHOW_GUESTS_TO_ALL_UI,
  SHOW_GUEST_TO_UI,
  SEND_MESSAGE_TO_UI,
  SEND_LOGOUT_TO_UI,
  CONNECT,
  DISCONNECT,
} from './messages/chatSocketIOMessages';

let chat;
let guests = {};
let messages = [];

const chatServer = {
  listen(server) {
    chat = io(server);

    chat.on(`connection`, s => {
      console.log(CONNECT);
      this.SERVER_SaveGuest(s);
      this.SERVER_SaveMessage(s);
      this.SERVER_CheckIfGuestExists(s);
      this.SERVER_Logout(s);
      this.SERVER_Disconnect(s);
    });
  },

  UI_ShowGuest(socket) {
    chat.emit(SHOW_GUESTS_TO_ALL_UI, guests);
    socket.emit(SHOW_GUEST_TO_UI, guests[socket.id]);
  },

  UI_SendMessage(message, socket) {
    chat.emit(SEND_MESSAGE_TO_UI, {
      message,
      guest: guests[socket.id]
    });
  },

  UI_SendLogout(socketId) {
    chat.emit(SEND_LOGOUT_TO_UI, socketId);
  },

  SERVER_SaveGuest(socket) {
    socket.on(SEND_GUEST_TO_SERVER, guest => {
      const checkIfGuestAlreadyExists = Object.values(guests).indexOf(guest) !== -1;

      if (!checkIfGuestAlreadyExists) {
        guests[socket.id] = guest;
        this.UI_ShowGuest(socket);
      }
    });
  },

  SERVER_SaveMessage(socket) {
    socket.on(SEND_MESSAGE_TO_SERVER, message => {
      messages.push(message);
      this.UI_SendMessage(message, socket);
    });
  },

  SERVER_CheckIfGuestExists(socket) {
    socket.on(SEND_IF_GUEST_EXISTS, guest => {
      const ifGuestExists = Object.values(guests).indexOf(guest) !== -1;
      socket.emit(SEND_GUEST_EXISTS, ifGuestExists);
    });
  },

  SERVER_Logout(socket) {
    socket.on(SEND_LOGOUT_REQUEST, () => {
      const socketId = socket.id;

      if (guests[socketId]) {
        this.UI_SendLogout(guests[socketId]);
        delete guests[socketId];
      }
    });
  },

  SERVER_Disconnect(socket) {
    socket.on(DISCONNECT, () => {
      const socketId = socket.id;

      if (guests[socketId]) {
        this.UI_SendLogout(guests[socketId]);
        delete guests[socketId];
      }
    });
  }
};

export default chatServer;
