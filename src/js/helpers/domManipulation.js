/* jshint esversion: 6 */
/* global document */

class DomManipulation {
    removeElement(element) {
        element.parentElement.removeChild(element);
    }

    addElement({ content, elementToAdd, whereToAdd, attribute, className }) {
        const element = document.createElement(elementToAdd);
        element.innerHTML = content;

        if (attribute) {
            element.setAttribute(attribute, ``);
        }

        if (className) {
            element.classList.add(className);
        }

        whereToAdd.append(element);
    }

    clearElement(elementToClear) {
        elementToClear.innerHTML = ``;
    }

    clearInput(input) {
        input.value = ``;
    }

    showElement(elementToShow, displayType) {
        elementToShow.classList.toggle(`hideElement`);

        switch (displayType) {
            case `block`:
                elementToShow.classList.add(`displayBlock`);
                break;
            case `flex`:
                elementToShow.classList.add(`displayFlex`);
                break;
            default:
                throw Error(`Incorrect display parameter`);
        }
    }

    hideElement(elementToHide) {
        if (this.checkIfElementContainsClass(elementToHide, `displayBlock`)) {
            elementToHide.classList.remove(`displayBlock`);
        }

        if (this.checkIfElementContainsClass(elementToHide, `displayFlex`)) {
            elementToHide.classList.remove(`displayFlex`);
        }

        elementToHide.classList.add(`hideElement`);
    }

    setElementsContent(elementToWrite, content) {
        elementToWrite.innerHTML = content;
    }

    checkIfElementContainsClass(element, className) {
        return element.classList.contains(className);
    }
}

export default DomManipulation;
