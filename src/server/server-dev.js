/* jshint esversion: 6 */
/* global __dirname, console, process */

import express from 'express';
import path from 'path';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../../webpack.dev.config.js';

const app = express();
import http from 'http';
const httpServer = http.Server(app);
const DIST_DIR = __dirname;
const HTML_FILE = path.join(DIST_DIR, 'index.html');
const port = process.env.PORT || 3001;
const compiler = webpack(config);

import chatServer from '../js/chatServer';
chatServer.listen(httpServer);

app.use(express.static(DIST_DIR));

app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath
}));

app.use(webpackHotMiddleware(compiler));

app.get('*', (request, response) => {
  response.sendFile(HTML_FILE);
});


httpServer.listen(port, () => {
  console.log(`localhost: ${port}`);
});
