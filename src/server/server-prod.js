/* jshint esversion: 6 */
/* global __dirname, console, process */

import express from 'express';
import path from 'path';

const app = express();
import http from 'http';
const httpServer = http.Server(app);
const DIST_DIR = __dirname;
const HTML_FILE = path.join(DIST_DIR, 'index.html');
const port = process.env.PORT || 3000;

import chatServer from '../js/chatServer';
chatServer.listen(httpServer);

app.use(express.static(DIST_DIR));

app.get('*', (request, response) => {
  response.sendFile(HTML_FILE);
});

httpServer.listen(port, () => {
  console.log(`localhost: ${port}`);
});
