/* jshint esversion: 6 */
/* global require, document */
import ChatUI from './js/chat/chatUI';
import ChatUIGuests from './js/chat/chatUIGuests';
import ChatUIMessages from './js/chat/chatUIMessages';
import '../src/css/style.css';
import {
  SHOW_GUESTS_TO_ALL_UI,
  SHOW_GUEST_TO_UI,
  SEND_MESSAGE_TO_UI,
  SEND_LOGOUT_TO_UI,
} from './js/messages/chatSocketIOMessages';

document.addEventListener(`DOMContentLoaded`, () => {
  const socket = require('socket.io-client')();
  const chatUI = new ChatUI();
  const chatUIGuests = new ChatUIGuests();
  const chatUIMessages = new ChatUIMessages();

  chatUI.init(socket);
  chatUIGuests.init(socket);
  chatUIMessages.init(socket);

  socket.on(SHOW_GUESTS_TO_ALL_UI, guests => chatUIGuests.UI_ShowGuestToAll(guests));
  socket.on(SHOW_GUEST_TO_UI, (guest) => chatUIGuests.UI_ShowGuest(guest));
  socket.on(SEND_MESSAGE_TO_UI, messageWithGuest => chatUIMessages.UI_ShowMessage(messageWithGuest));
  socket.on(SEND_LOGOUT_TO_UI, guest => chatUI.UI_LogoutAction(guest));
});
